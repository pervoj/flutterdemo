import "package:flutter/material.dart";

import "pages/camera.dart";
import "pages/gps.dart";
import "pages/home.dart";
import "pages/loading.dart";
import "services/camera.dart";
import "services/paths.dart";

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Camera.init();
  await Paths.init();

  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter Demo",
      theme: ThemeData.light(useMaterial3: true),
      darkTheme: ThemeData.dark(useMaterial3: true),
      routes: {
        "/": (_) => const LoadingPage(),
        "/home": (_) => const HomePage(),
        "/gps": (_) => const GPSPage(),
        "/camera": (_) => const CameraPage(),
      },
    );
  }
}

import "package:flutter/material.dart";

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flutter Demo"),
      ),
      body: GridView(
        padding: const EdgeInsets.all(20),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 20,
        ),
        children: [
          _CardButton(
            title: "GPS",
            icon: Icons.gps_fixed,
            callback: () {
              Navigator.pushNamed(context, "/gps");
            },
          ),
          _CardButton(
            title: "Fotoaparát",
            icon: Icons.camera_alt,
            callback: () {
              Navigator.pushNamed(context, "/camera");
            },
          ),
        ],
      ),
    );
  }
}

class _CardButton extends StatelessWidget {
  const _CardButton({
    required this.icon,
    required this.title,
    required this.callback,
  });

  final IconData icon;
  final String title;
  final void Function() callback;

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      child: ElevatedButton(
        onPressed: callback,
        style: const ButtonStyle(
          shape: MaterialStatePropertyAll(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(16)),
            ),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              icon,
              size: 48,
            ),
            const SizedBox(height: 24),
            Text(
              title,
              style: const TextStyle(fontSize: 18),
            ),
          ],
        ),
      ),
    );
  }
}

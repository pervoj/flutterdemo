import "dart:io";

import "package:camera/camera.dart";
import "package:flutter/material.dart";

import "../services/camera.dart";

class CameraPage extends StatelessWidget {
  const CameraPage({super.key});

  Future<void> _previewImage(BuildContext context, File file) {
    return showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Náhled fotky"),
          content: Image.file(file),
          actions: [
            TextButton(
              child: const Text("Zavřít"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = CameraController(
      Camera.cameras[0],
      ResolutionPreset.max,
      enableAudio: false,
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text("Fotoaparát"),
      ),
      body: CameraWidget(
        cameraController: controller,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        icon: const Icon(Icons.camera),
        label: const Text("Vyfotit"),
        onPressed: () {
          controller.takePicture().then((picture) {
            _previewImage(context, File(picture.path));
          });
        },
      ),
    );
  }
}

import "dart:async";
import "dart:io";

import "package:flutter/material.dart";

import "../services/gps.dart";

class GPSPage extends StatefulWidget {
  const GPSPage({super.key});

  @override
  State<GPSPage> createState() => _GPSPageState();
}

class _GPSPageState extends State<GPSPage> {
  late final DisposableBuildContext _disposableContext;
  late final StreamSubscription? updateSubscription;

  GpsPosition? _position;

  bool? _gpsDisabled;

  Future<bool> _gpsEnabled() async {
    if (await Gps.isEnabled()) return true;
    await Gps.openEnableDialog(_disposableContext.context!);
    return await Gps.isEnabled();
  }

  Future<void> _gpsStart() async {
    await Gps.initForegroundService();
    if (!await Gps.startForegroundService()) return;

    updateSubscription = Gps.onForegroundServiceUpdate().listen(
      (position) {
        setState(() {
          _position = position;
        });
      },
    );
  }

  Future<void> _initGps() async {
    final enabled = await _gpsEnabled();
    if (enabled) await _gpsStart();
    _gpsDisabled = !enabled;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _disposableContext = DisposableBuildContext(this);
    _initGps();
  }

  @override
  void dispose() {
    _disposableContext.dispose();
    updateSubscription?.cancel();
    super.dispose();
  }

  void showLogDialog(BuildContext context, void Function() callback) {
    final log = Gps.logFile.readAsStringSync(encoding: const SystemEncoding());

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: Text(
              log,
              softWrap: false,
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                callback();
              },
              child: const Text("Zavřít"),
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("GPS"),
      ),
      body: Center(
        child: Card(
            child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Aktuální pozice",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(height: 8),
              Text(
                _gpsDisabled == true
                    ? "Určování polohy je vypnuto!"
                    : (_position == null
                        ? "Načítání…"
                        : "zeměpisná šířka: ${_position!.latitude}\n"
                            "zeměpisná délka: ${_position!.longitude}"),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        )),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        label: const Text("Stop"),
        icon: const Icon(Icons.stop),
        onPressed: () {
          updateSubscription?.cancel();
          Gps.stopForegroundService();

          showLogDialog(context, () {
            Navigator.of(context).pop();
          });
        },
      ),
    );
  }
}

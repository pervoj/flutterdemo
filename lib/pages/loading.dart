import "package:flutter/material.dart";

import "../services/permissions.dart";

class LoadingPage extends StatefulWidget {
  const LoadingPage({super.key});

  @override
  State<LoadingPage> createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  late final DisposableBuildContext _disposableContext;

  Future<void> _request() async {
    final context = _disposableContext.context!;

    await Permissions.notifications.request(
      mandatory: true,
      ifNotGranted: () async {
        await Permissions.notifications.openErrorDialog(context);
      },
    );

    await Permissions.locationInUse.request(
      mandatory: true,
      ifNotGranted: () async {
        await Permissions.locationInUse.openErrorDialog(context);
      },
    );

    while (!await Permissions.locationAlways.granted) {
      await Permissions.openDialog(
        context: _disposableContext.context!,
        title: "Poloha na pozadí",
        content: "Aplikace pro svůj běh vyžaduje oprávnění pro určování polohy "
            " na pozadí. Tlačítko níže vás přesměruje do nastavení, kde bude "
            "nutné přepnout možnost „Povolit při používání aplikace“ na "
            "„Povolit vždy“.",
        settingsPanel: Permissions.locationAlways.settingsPanel,
        refresh: () {},
      );
    }

    await Permissions.camera.request(
      mandatory: true,
      ifNotGranted: () async {
        await Permissions.camera.openErrorDialog(context);
      },
    );
  }

  void _switchToHomePage() {
    Navigator.of(_disposableContext.context!).pushReplacementNamed("/home");
  }

  @override
  void initState() {
    super.initState();
    _disposableContext = DisposableBuildContext(this);

    _request().then((_) {
      _switchToHomePage();
    });
  }

  @override
  void dispose() {
    _disposableContext.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const CircularProgressIndicator(),
              const SizedBox(height: 24),
              Text(
                "Flutter Demo",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(height: 6),
              Text(
                "Načítání…",
                style: Theme.of(context).textTheme.bodySmall,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import "dart:io";

import "package:package_info_plus/package_info_plus.dart";
import "package:path/path.dart" as path;

// FIXME: Works only for Android!

class Paths {
  Paths._();
  static PackageInfo? _packageInfo;

  static Future<void> init() async {
    _packageInfo ??= await PackageInfo.fromPlatform();
  }

  static Directory join(Directory parent, String childName) {
    return Directory(path.join(parent.path, childName));
  }

  static Directory ensureCreated(Directory directory) {
    if (directory.existsSync()) return directory;
    directory.createSync(recursive: true);
    return directory;
  }

  static Directory getDataDir() {
    return ensureCreated(Directory("/data/data/${_packageInfo!.packageName}"));
  }

  static Directory getDataDbDir() {
    return ensureCreated(join(getDataDir(), "databases"));
  }

  static Directory getDataLibDir() {
    return ensureCreated(join(getDataDir(), "lib"));
  }

  static Directory getDataFilesDir() {
    return ensureCreated(join(getDataDir(), "files"));
  }

  static Directory getDataPrefsDir() {
    return ensureCreated(join(getDataDir(), "shared_prefs"));
  }

  static Directory getDataCacheDir() {
    return ensureCreated(join(getDataDir(), "cache"));
  }
}

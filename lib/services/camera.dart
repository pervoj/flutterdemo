import "package:camera/camera.dart";
import "package:flutter/material.dart";

class Camera {
  static late List<CameraDescription> cameras;

  static Future<void> init() async {
    cameras = await availableCameras();
  }
}

class CameraWidget extends StatefulWidget {
  const CameraWidget({super.key, required this.cameraController});

  final CameraController cameraController;

  @override
  State<CameraWidget> createState() => _CameraWidgetState();
}

class _CameraWidgetState extends State<CameraWidget> {
  late final CameraController controller;

  @override
  void initState() {
    super.initState();
    controller = widget.cameraController;
    controller.initialize().then(
      (_) {
        if (!mounted) return;
        setState(() {});
      },
      onError: (e) {
        if (e is CameraException) {
          switch (e.code) {
            case "CameraAccessDenied":
              // TODO: Handle access errors here.
              break;
            default:
              // TODO: Handle other errors here.
              break;
          }
        }
      },
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: controller.value.isInitialized
          ? CameraPreview(controller)
          : const Center(child: CircularProgressIndicator()),
    );
  }
}

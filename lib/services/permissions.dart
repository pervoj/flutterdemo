import "dart:async";

import "package:flutter/material.dart";
import "package:open_apps_settings/open_apps_settings.dart";
import "package:open_apps_settings/settings_enum.dart";
import "package:permission_handler/permission_handler.dart";

class Permissions {
  static final notifications = PermissionData(
    permission: Permission.notification,
    errorMessageTitle: "Oznámení",
    errorMessageContent: "Oprávnění pro zobrazování oznámení je vyžadováno!",
    settingsPanel: SettingsCode.NOTIFICATION,
  );

  static final locationInUse = PermissionData(
    permission: Permission.locationWhenInUse,
    errorMessageTitle: "Poloha",
    errorMessageContent: "Oprávnění pro určování polohy je vyžadováno!",
    settingsPanel: SettingsCode.LOCATION,
  );

  static final locationAlways = PermissionData(
    permission: Permission.locationAlways,
    errorMessageTitle: "Poloha na pozadí",
    errorMessageContent:
        "Oprávnění pro určování polohy na pozadí je vyžadováno!",
    settingsPanel: SettingsCode.LOCATION,
  );

  static final camera = PermissionData(
    permission: Permission.camera,
    errorMessageTitle: "Fotoaparát",
    errorMessageContent: "Oprávnění pro přístup k fotoaparátu je vyžadováno!",
    settingsPanel: SettingsCode.APP_SETTINGS,
  );

  Permissions._();

  static Future<void> openDialog({
    required BuildContext context,
    required String title,
    required String content,
    SettingsCode? settingsPanel,
    FutureOr<void> Function()? openSettings,
    FutureOr<void> Function()? refresh,
  }) async {
    await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: [
            if (refresh != null)
              TextButton.icon(
                icon: const Icon(Icons.refresh),
                label: const Text("Ověřit znovu"),
                onPressed: () {
                  Navigator.of(context).pop();
                  refresh();
                },
              ),
            TextButton.icon(
              icon: const Icon(Icons.settings),
              label: const Text("Otevřít nastavení"),
              onPressed: () {
                if (settingsPanel != null) {
                  OpenAppsSettings.openAppsSettings(
                    settingsCode: settingsPanel,
                  );
                } else {
                  if (openSettings != null) openSettings();
                }
              },
            ),
          ],
        );
      },
    );
  }
}

class PermissionData {
  final Permission permission;
  final String errorMessageTitle;
  final String errorMessageContent;
  final SettingsCode settingsPanel;

  PermissionData({
    required this.permission,
    required this.errorMessageTitle,
    required this.errorMessageContent,
    required this.settingsPanel,
  });

  Future<bool> get granted => permission.isGranted;

  Future<bool> request({
    bool mandatory = false,
    FutureOr<void> Function()? ifNotGranted,
  }) async {
    do {
      if (await permission.request().isGranted) return true;
      if (ifNotGranted != null) await ifNotGranted();
    } while (mandatory);
    return false;
  }

  Future<void> openErrorDialog(
    BuildContext context, [
    FutureOr<void> Function()? refresh,
  ]) {
    return Permissions.openDialog(
      context: context,
      title: errorMessageTitle,
      content: errorMessageContent,
      settingsPanel: settingsPanel,
      refresh: refresh ?? () {},
    );
  }
}

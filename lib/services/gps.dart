import "dart:io";

import "package:flutter/material.dart";
import "package:flutter_background_service/flutter_background_service.dart";
import "package:flutter_local_notifications/flutter_local_notifications.dart";
import "package:geolocator/geolocator.dart";
import "package:open_apps_settings/settings_enum.dart";
import "package:path/path.dart";

import "paths.dart";
import "permissions.dart";

File getGpsLogFile() {
  final file = File(join(Paths.getDataLibDir().path, "gps.log"));
  if (!file.existsSync()) file.createSync();
  return file;
}

class GpsPosition {
  final double longitude;
  final double latitude;

  GpsPosition({
    required this.longitude,
    required this.latitude,
  });

  Map<String, dynamic> toJson() => {
        "longitude": longitude,
        "latitude": latitude,
      };

  @override
  String toString() => toJson().toString();

  factory GpsPosition.fromMap(Map<String, dynamic> map) => GpsPosition(
        longitude: map["longitude"],
        latitude: map["latitude"],
      );
}

class Gps {
  Gps._();

  static File get logFile {
    final file = File(join(Paths.getDataLibDir().path, "gps.log"));
    if (!file.existsSync()) file.createSync();
    return file;
  }

  static Future<bool> isEnabled() async {
    return await Geolocator.isLocationServiceEnabled();
  }

  static Future<void> openEnableDialog(BuildContext context) {
    return Permissions.openDialog(
      context: context,
      title: "Určování polohy",
      content: "Pro běh aplikace je nutné zapnout službu určování polohy.",
      settingsPanel: SettingsCode.LOCATION,
    );
  }

  static late FlutterBackgroundService _service;

  static Future<void> initForegroundService() async {
    _service = FlutterBackgroundService();

    const channel = AndroidNotificationChannel(
      "fluttertest_fg_service_gps_8049",
      "Flutter Demo",
      description: "Snímání polohy pokračuje i na pozadí.",
      importance: Importance.defaultImportance,
    );

    final plugin = FlutterLocalNotificationsPlugin();

    plugin.initialize(
      const InitializationSettings(
        iOS: DarwinInitializationSettings(),
        android: AndroidInitializationSettings("ic_bg_service_small"),
      ),
    );

    plugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await _service.configure(
      androidConfiguration: AndroidConfiguration(
        isForegroundMode: true,
        autoStart: false,
        autoStartOnBoot: false,
        onStart: _onStart,
        notificationChannelId: channel.id,
        foregroundServiceNotificationId: 8049,
        initialNotificationTitle: channel.name,
        initialNotificationContent: channel.description!,
      ),
      iosConfiguration: IosConfiguration(
        autoStart: false,
        onForeground: _onStart,
      ),
    );
  }

  static Future<bool> isForegroundServiceRunning() => _service.isRunning();
  static void stopForegroundService() => _service.invoke("stopService");
  static Future<bool> startForegroundService() async {
    if (await isForegroundServiceRunning()) return true;
    return await _service.startService();
  }

  static Stream<GpsPosition> onForegroundServiceUpdate() {
    return _service.on("update").map<GpsPosition>((event) {
      return GpsPosition.fromMap(event!["position"] as Map<String, dynamic>);
    });
  }

  @pragma("vm:entry-point")
  static void _onStart(ServiceInstance service) async {
    await Paths.init();
    logFile.writeAsStringSync("");

    service.on("stopService").listen((event) {
      service.stopSelf();
    });

    Geolocator.getPositionStream().listen((positionData) async {
      final position = GpsPosition(
        latitude: positionData.latitude,
        longitude: positionData.longitude,
      );

      logFile.writeAsStringSync(
        "${DateTime.now()}\nŠ: ${position.latitude}; D: ${position.longitude}\n\n",
        mode: FileMode.append,
        encoding: const SystemEncoding(),
      );

      service.invoke("update", {"position": position.toJson()});
    });
  }
}
